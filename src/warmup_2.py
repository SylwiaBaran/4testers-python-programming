def calculate_second_power_of_number(number):
    return number ** 2


def convert_celsius_to_fahrenheit(temperature_in_celsius):
    return 32 + 1.8 * temperature_in_celsius


def calculate_volume_of_cuboid(a, b, h):
    return a * b * h


if __name__ == '__main__':
    # Zadanie 1
    print(calculate_second_power_of_number(0))
    print(calculate_second_power_of_number(16))
    print(calculate_second_power_of_number(2.55))

    # Zadanie 2
    print(convert_celsius_to_fahrenheit(20))

    # Zadanie 3

    print(calculate_volume_of_cuboid(3, 5, 7))
