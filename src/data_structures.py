movies = ['Dune', 'Star Wars', 'Blade Runner', 'Stalker', 'Lost Highway']

last_movie = movies[-1]
movies.append('LOTR')
movies.append('Titanic')
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'Top Gun 2')
print(movies)

emails = ['a@example.com', 'b@example.com']
print(len(emails))
first_element = emails[0]
print(first_element)
last_element = emails[-1]
print(last_element)

friend = {
    "first_name": "Michał",
    "age": 33,
    "hobby": ["sport", "motoryzacja"]}

friend_hobbies = friend["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"].append("gotowanie")
print(friend)
friend["married"] = True
print(friend)


