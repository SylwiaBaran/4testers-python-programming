import random
import string


def calculate_average_of_list_numbers(input_list):
    return sum(input_list) / len(input_list)


def calculate_average_of_two_numbers(a, b):
    return (a + b) / 2


def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


def generate_login_data_for_email(email):
    random_password = generate_random_password()
    return {"email": email, "password": random_password}


def describe_player(player_dictionary):
    nick = player_dictionary["nick"]
    type = player_dictionary["type"]
    exp_points = player_dictionary["exp_points"]

    print(f"The player '{nick}' is of type {type.capitalize()} and has {exp_points}EXP")


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_list_numbers(january)
    february_average = calculate_average_of_list_numbers(february)
    bimonthly_average = calculate_average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)

    print(generate_login_data_for_email("adam@example.com"))
    print(generate_login_data_for_email("michal@example.com"))
    print(generate_login_data_for_email("sylwia@example.com"))

    player1 = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    describe_player(player1)
