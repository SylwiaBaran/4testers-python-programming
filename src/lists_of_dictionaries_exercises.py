adresses = [
    {"city": "Warsaw", "street": "Puławska", "house_number": 100, "post_code": "02-670"},
    {"city": "Kielce", "street": "Andersa", "house_number": 36, "post_code": "25-217"},
    {"city": "Piaseczno", "street": "Jana Pawła II", "house_number": 82, "post_code": "05-500"}
]

if __name__ == '__main__':
    print(adresses[-1]["post_code"])
    print(adresses[1]["city"])

    adresses[0]["street"] = "Domaniewska"
    print(adresses)
