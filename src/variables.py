first_name = "Sylwia"
last_name = "Baran"
age = 32

print(first_name)
print(last_name)
print(age)

friend_name = "Anna"
friend_age = 32
friend_number_of_pets = 1
friend_has_driving_license = False
friendship_duration_in_years = 16.5

print("Friend's name:", friend_name, sep='\t')

print(friend_name, friend_age, friend_number_of_pets, friend_has_driving_license, friendship_duration_in_years, sep=',')
