def display_speed_information(speed):
    if speed > 50:
        print("Slow down! :(")
    else:
        print("Thank you, your speed is below limit! :)")


def check_weather_conditions(celsius, hectopascal):
    if celsius == 0 and hectopascal == 1013:
        return True
    else:
        return False


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print("Your speed was:", speed)
    if speed > 100:
        print("You just lost your driving license :(")
    elif speed > 50:
        print(f"You just got a fine! Fine amount {calculate_fine_amount(speed)}")
    else:
        print("Thank you, your speed is fine!")


def return_grade_description(grade):
    if not (isinstance(grade, float) or isinstance(grade, int)):
        return "N/A"
    elif grade < 2 or grade > 5:
        return "N/A"
    elif grade >= 4.5:
        return "bardzo dobry"
    elif grade >= 4.0:
        return "dobry"
    elif grade >= 3.0:
        return "dostateczny"
    else:
        return "niedostateczny"




if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(51)
    display_speed_information(49)

    print(check_weather_conditions(0, 1013))
    print(check_weather_conditions(1, 1013))
    print(check_weather_conditions(0, 1014))
    print(check_weather_conditions(1, 1014))

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(51)
    print_the_value_of_speeding_fine_in_built_up_area(70)

    print(return_grade_description(5))
    print(return_grade_description(10))
    print(return_grade_description(4.5))
    print(return_grade_description(3.2))
    print(return_grade_description(2))
    print(return_grade_description(3))
    print(return_grade_description(1.2))
    print(return_grade_description("bdb"))
