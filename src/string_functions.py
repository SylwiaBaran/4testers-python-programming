def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}! ")


def generate_email_for_4testerspl(person_first_name, person_last_name):
    print(f"{person_first_name}.{person_last_name}@4testers.pl")


if __name__ == '__main__':
    generate_email_for_4testerspl("janusz", "nowak")
    generate_email_for_4testerspl("barbara", "nowak")

if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Kasia", "Szczecin")
    print_greetings_for_a_person_in_the_city("Adam", "Poznań")
